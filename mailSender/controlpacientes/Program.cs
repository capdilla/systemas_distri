﻿
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace controlpacientes
{
    public class Program
    {
        public static MailDates data;

        public static void Main(string[] args)
        {


            data = APICall.GetData();

            RabbitReceiver receiver = new RabbitReceiver();
            receiver.ReceiveData(data);

            Stopwatch sw = new Stopwatch();
            sw.Start();

            Checker checker = new Checker();
            while (true)
            {
                //count++;
                //Console.WriteLine("Vez numero: " + count + " y data: " + data.Data.Count);

                Thread.Sleep(1000);
                Parallel.ForEach(data.Data, (item) =>
                {
                    DateTime convertedDateTime = Convert.ToDateTime(item.Date.Substring(0, 10)
                                                                    + " " + item.Date.Substring(11, 8));
                    checker.CheckDate(convertedDateTime, item);
                });
            }
        }
    }
}
