﻿using System.Collections.Generic;

namespace controlpacientes
{
    public class MailDates
    {
        public List<Data> Data { get; set; }
    }

    public class Data
    {
        public string Date { get; set; }
        public Paciente Paciente { get; set; }
    }

    public class Paciente
    {
        public string Email { get; set; }
    }
}