﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;


namespace controlpacientes
{
    public class RabbitReceiver
    {
        public static IModel channel;
        public static EventingBasicConsumer consumer;
        public static Data jsonRec = null;
        public RabbitReceiver()
        {


            string host = "3.17.68.196";

            if (Environment.GetEnvironmentVariable("WEB") != null)
            {
                Console.WriteLine("waiting");
                Thread.Sleep(3000);
                host = Environment.GetEnvironmentVariable("RABBIT");
                Console.WriteLine("DONE");
            }

            var factory = new ConnectionFactory()
            {
                HostName = host,
                Port = 5672,
                UserName = "user",
                Password = "password",
                VirtualHost = "/"
            };
            var connection = factory.CreateConnection();
            channel = connection.CreateModel();

            channel.QueueDeclare(queue: "hello",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

            consumer = new EventingBasicConsumer(channel);
        }

        public void ReceiveData(MailDates data)
        {
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body);
                var json = JsonConvert.DeserializeObject<Data>(message);

                bool response = true;
                foreach (var item in data.Data)
                {
                    if (item.Date == json.Date && item.Paciente.Email == json.Paciente.Email)
                        response = false;
                }
                if (response)
                    data.Data.Add(json);

                Console.WriteLine(" [x] Received {0}", json.Paciente.Email);
            };

            channel.BasicConsume(queue: "hello",
                                 autoAck: true,
                                 consumer: consumer);
        }
    }
}
