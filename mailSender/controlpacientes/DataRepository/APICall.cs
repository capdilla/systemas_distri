﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;


namespace controlpacientes
{
    public class APICall
    {
        public static MailDates GetData()
        {
            Thread.Sleep(3000);
            string url = "localhost";

            if (Environment.GetEnvironmentVariable("WEB") != null)
            {
                Console.WriteLine("waiting");
                Thread.Sleep(3000);
                url = Environment.GetEnvironmentVariable("WEB");
                Console.WriteLine("DONE");
            }

            string httpURl = "http://" + url;
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(httpURl)
            };

            // Add an Accept header for JSON format.
            client
            .DefaultRequestHeaders
            .Accept
            .Add(new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync("/api/dates/api_dates").Result;

            var dataObj = response.Content.ReadAsStringAsync().Result;

            var json = JsonConvert.DeserializeObject<MailDates>(dataObj);

            Console.WriteLine("DATA GET SUSEESSSSSSS");

            return json;
        }
    }
}
