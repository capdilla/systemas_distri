﻿using controlpacientes.EmailSender;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace controlpacientes
{
    public class Checker
    {
        public delegate Task SendMailHandler(string destinatary, string body);
        public static event SendMailHandler ReminderMail;
        public Checker()
        {
            ReminderMail += SendMail.EnviarCorreo;
        }

        public async void CheckDate(DateTime time, Data dataPaciente)
        {

            if (DateTime.Now.Hour == time.Hour - 2 && DateTime.Now.Minute == time.Minute && DateTime.Now.Second == time.Second)
            {
                string body = "Su cita medica es el dia " + time.ToLongDateString() + " a las " + time.ToShortTimeString();
                ReminderMail(dataPaciente.Paciente.Email, body);
            }
        }
    }
}
