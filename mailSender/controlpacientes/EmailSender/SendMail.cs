﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace controlpacientes.EmailSender
{
    public class SendMail
    {
        public static async Task EnviarCorreo(string destinatary, string body)
        {
            Console.WriteLine("Enviando correo");

            var fromAddress = new MailAddress("avisosmedicospr@gmail.com", "Medico");
            var toAddress = new MailAddress(destinatary);
            string subject = "Recordatorio de cita medica";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, "avisos123"),
            };

            var message = new MailMessage(fromAddress.Address, toAddress.Address, subject, body);

            try
            {
                smtp.SendMailAsync(message);
                Console.WriteLine("Correo de información enviado");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ha habido un error: " + ex.Message);
            }
        }
    }
}
