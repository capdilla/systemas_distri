const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const cors = require('kcors');
const jwt = require('koa-jwt');
var views = require('koa-views');
const session = require('koa-session');
const passport = require('koa-passport');
const serve = require('koa-static');
const { controllerRoutes } = require('koa2-controller-router')

// const { controllerRoutes } = require('koa2-controller')
const { rbac, authentication, handleJWTErrors } = require('./app/middelware.js');
const privateKey = require('fs').readFileSync(__dirname + '/app/cert/public.key');

const app = new Koa();
const PORT = process.env.PORT || 3000;

var server = require('http').createServer(app.callback())
var io = require('socket.io')(server)

app.keys = ['hello'];

// const SocketIOManager = require('./app/SocketIOManager');

// const User = require('./app/models/User');



// require('./app/models/migrations')//run migrations

// const IO = new SocketIOManager(io);


// IO.createSockets('plug');

// initialize public  routes
const publicRoutes = new controllerRoutes({
  absolutePath: __dirname + '/app/controllers/public/',
}).routes()

const apis = new controllerRoutes({
  absolutePath: __dirname + '/app/controllers/apis/',
  prefix: '/api'
}).routes()

//rutas privadas con seguridad
const privateRoutes = new controllerRoutes({
  absolutePath: __dirname + '/app/controllers/private/',
}).routes()

// console.log(privateRoutes.router)
//utilizamos todas las librerias


app
  .use(serve(__dirname + '/app/static'))
  .use(views(__dirname + '/app/template', { map: { html: 'ejs' } }))
  .use(bodyParser())
  .use(cors())
  .use(session({}, app))
  .use(apis)
  .use(publicRoutes)
  // .use(passport.initialize())
  // .use(passport.session())
  // .use(jwt({ secret: privateKey }).unless({}))
  .use(handleJWTErrors)
  .use(authentication)
  .use(rbac)
  .use(privateRoutes)


server.listen(PORT, () => {
  console.log(`listen port ${PORT}`)
});


