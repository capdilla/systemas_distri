var amqp = require('amqplib/callback_api');

const def = {
  protocol: 'amqp',
  hostname: 'localhost',
  port: 5672,
  username: 'user',
  password: 'password',
  vhost: '/'
};

const conn = process.env.RABBIT || def

module.exports = (message) => {
  amqp.connect(conn, function (err, conn) {

    if (err)
      return

    conn.createChannel(function (err, ch) {
      var q = 'hello';
      var msg = JSON.stringify(message.models);

      ch.assertQueue(q, { durable: false });
      ch.sendToQueue(q, Buffer.from(msg));
      console.log(" [x] Sent %s", msg);
    });
    // setTimeout(function () { conn.close(); process.exit(0) }, 500);
  });
}
