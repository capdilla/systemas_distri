const generic = {
  host: process.env.DATABASE_HOST || 'localhost',
  database: '',
  dialect: 'mysql',
  username: 'root',
  password: 'secret',
  port: 3306
}

module.exports = {
  dev: {
    ...generic,
    database: 'sistemas_distri',
    host: 'dev.programacion.com'
  },
  test: {
    ...generic
  },
  prod: {
    ...generic,
    database: 'sistemas_distri',
  }
}