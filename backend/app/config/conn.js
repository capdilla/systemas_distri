const Sequelize = require('sequelize');
const db = require('../config/db');

const { ENV } = process.env

const currentENV = ENV || "dev"

const { database, username, password, dialect, host, port } = db[currentENV];

module.exports = new Sequelize(database, username, password, {
  host,
  dialect,
  port,
  operatorsAliases: false,
  logging: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});