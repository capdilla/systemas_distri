module.exports = (sequelize, DataTypes) => {
  var Dates = sequelize.define('dates', {
    date: DataTypes.DATE
  });

  Dates.associate = function (models) {
    Dates.belongsTo(models.paciente);
    models.paciente.hasMany(Dates);
    // Dates.hasMany(models.paciente, { foreignKey: 'paciente_id' })
    // models.paciente.hasMany(Dates, { foreignKey: 'paciente_id' });
  };

  return Dates;
}


