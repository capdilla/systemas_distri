module.exports = (sequelize, DataTypes) => {
  const Pacientes = sequelize.define('paciente', {
    nombre: {
      type: DataTypes.STRING
    },
    email: {
      type: DataTypes.STRING
    },
    edad: {
      type: DataTypes.STRING
    },
  });

  Pacientes.associate = (models) => {
    // Pacientes.hasMany(models.user, { foreignKey: "user_id" })
    models.user.hasMany(Pacientes)
    Pacientes.belongsTo(models.user)
    // Pacientes.belongsTo(models.dates)
  }
  return Pacientes;
}
