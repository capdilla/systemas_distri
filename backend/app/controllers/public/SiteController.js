const { koa2Controller } = require('koa2-controller-router');
const models = require('../../models');
const Users = models.user;
const jwt = require('jsonwebtoken');
const fs = require('fs')

const privateKey = fs.readFileSync(require('path').join(__dirname, '../../cert/private.key'));


module.exports = class SiteController extends koa2Controller {

  constructor() {
    super();
    // this.prefix = "/k";
  }

  /**
    * get all the records and render index
    * @param {any} ctx 
    * @param {any} next 
    */
  async getIndex(ctx, next) {
    if (ctx.session.isNew) {
      return ctx.render("login.ejs")
    }

    return ctx.renderView("index.ejs");
  }

  /**
    * make login in the app
    * @param {int} id 
    */
  async postLogin(ctx, next) {
    const model = await Users.findOne({ where: ctx.request.body })
    if (model == null) {

    } else {
      const claim = {
        payload: { user: model }
      }

      ctx.session.user = {
        user: model,
        token: jwt.sign(claim, privateKey, { algorithm: 'RS256' })
      };
    }

    return ctx.redirect('/')
  }


  async getLogin(ctx, next) {
    return ctx.render("login.ejs")
  }

  /**
    * render create form
    * @param {any} ctx 
    * @param {any} next 
    */
  async getRegister(ctx, next) {
    return ctx.render("register");
  }

  /**
    * create a new record
    * @param {any} ctx 
    * @param {any} next 
    */
  async postCreate_user(ctx, next) {
    Users.create(ctx.request.body);
    return ctx.redirect("/");
  }

  /**
    * render form for edit one record
    * @param {int} id 
    * @param {any} ctx 
    */
  async getLogout(ctx) {
    ctx.session = null;
    return ctx.redirect("/");
  }

  /**
    * update one record
    * @param {int} id 
    * @param {any} ctx 
    */
  async postUpdate(id, ctx) {

  }

  /**
    * delete one record
    * @param {int} id 
    * @param {any} ctx 
    */
  async allDelete(id, ctx) {

  }

}