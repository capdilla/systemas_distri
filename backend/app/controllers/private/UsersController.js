const { koa2Controller } = require('koa2-controller-router');
const models = require('../../models');
const Users = models.user

module.exports = class UsersController extends koa2Controller {

  constructor() {
    super();
    this.prefix = "/users";
  }

  /**
    * get all the records and render index
    * @param {any} ctx 
    * @param {any} next 
    */
  async getIndex(ctx, next) {
    const models = await Users.findAll();
    return ctx.renderView("index.ejs", { models });
  }

  /**
    * get one record and render view
    * @param {int} id 
    * @param {any} ctx 
    */
  async getView(id, ctx) {

  }

  /**
    * render create form
    * @param {any} ctx 
    * @param {any} next 
    */
  async getNew(ctx, next) {
    return ctx.renderView("create.ejs");
  }

  /**
    * create a new record
    * @param {any} ctx 
    * @param {any} next 
    */
  async postCreate(ctx, next) {
    Users.create(ctx.request.body);
    return ctx.redirect(this.prefix);
  }

  /**
    * render form for edit one record
    * @param {int} id 
    * @param {any} ctx 
    */
  async getEdit(id, ctx) {
    const model = await Users.findById(id);
    return ctx.renderView("update.ejs", { model });
  }

  /**
    * update one record
    * @param {int} id 
    * @param {any} ctx 
    */
  async postUpdate(id, ctx) {
    const model = await Users.findById(id);
    await model.update(ctx.request.body);
    return ctx.redirect(this.prefix);
  }

  /**
    * delete one record
    * @param {int} id 
    * @param {any} ctx 
    */
  async allDelete(id, ctx) {
    const model = await Users.findById(id);
    await model.destroy();
    return ctx.redirect(this.prefix);
  }

}