const { koa2Controller } = require('koa2-controller-router');
const { Op } = require('sequelize')
const moment = require('moment')
const model = require('../../models');
const Dates = model.dates;
const Pacientes = model.paciente;

const sendRabbit = require('../../rabbitmq/send');

module.exports = class DateController extends koa2Controller {

  constructor() {
    super();
    this.prefix = "/dates";
  }

  /**
    * get all the records and render index
    * @param {any} ctx 
    * @param {any} next 
    */
  async getIndex(ctx, next) {
    const { id } = ctx.session.user.user
    const models = await Dates.findAll({
      include: [
        {
          model: model.paciente,
          require: true,
          where: { userId: id }
          // include: [{
          //   model: model.user,
          //   where: { id }
          // }]
        }]
    });
    return ctx.renderView("index.ejs", { models });
  }

  /**
    * get one record and render view
    * @param {int} id 
    * @param {any} ctx 
    */
  async getView(id, ctx) {

  }

  /**
    * render create form
    * @param {any} ctx 
    * @param {any} next 
    */
  async getNew(ctx, next) {
    const { id } = ctx.session.user.user

    const models = await Pacientes.findAll({ where: { userId: id } })
    return ctx.renderView("create.ejs", { pacientes: models });
  }

  /**
    * create a new record
    * @param {any} ctx 
    * @param {any} next 
    */
  async postCreate(ctx, next) {
    let { date, time } = ctx.request.body;

    let newDate = moment(`${date} ${time}`).format("YYYY-MM-D HH:mm:ss")

    const body = {
      ...ctx.request.body,
      date: newDate
    }

    const mm = await Dates.create(body);

    const models = await Dates.findAll({
      include: [
        {
          model: model.paciente,
          require: true,
          attributes: ['email']
        }],
      where: {
        id: mm.id
      },
      attributes: {
        exclude: ['pacienteId', 'id', 'createdAt', 'updatedAt']
      }
    })

    sendRabbit({ models: models[0] })
    return ctx.redirect(this.prefix);
  }

  /**
    * render form for edit one record
    * @param {int} id 
    * @param {any} ctx 
    */
  async getEdit(id, ctx) {
    const model = await Dates.findById(id);
    return ctx.renderView("update.ejs", { model });
  }

  /**
    * update one record
    * @param {int} id 
    * @param {any} ctx 
    */
  async postUpdate(id, ctx) {
    const model = await Dates.findById(id);
    await model.update(ctx.request.body);
    return ctx.redirect(this.prefix);
  }

  /**
    * delete one record
    * @param {int} id 
    * @param {any} ctx 
    */
  async allDelete(id, ctx) {
    const model = await Dates.findById(id);
    await model.destroy();
    return ctx.redirect(this.prefix);
  }

}