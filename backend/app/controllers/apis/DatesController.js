const { koa2Controller } = require('koa2-controller-router');
const { Op } = require('sequelize')
const moment = require('moment')
const model = require('../../models');
const Dates = model.dates;
const Pacientes = model.paciente;

module.exports = class DateController extends koa2Controller {

  constructor() {
    super();
    this.prefix = "/dates";
  }

  async getApi_dates(ctx) {

    const models = await Dates.findAll({
      include: [
        {
          model: model.paciente,
          require: true,
          attributes: ['email']
        }],
      where: {
        date: {
          [Op.gte]: new Date()
        }
      },
      attributes: {
        exclude: ['pacienteId', 'id', 'createdAt', 'updatedAt']
      }
    })
    return ctx.body = {
      data: models
    }

  }

}