var amqp = require('amqplib/callback_api');

amqp.connect({ protocol: 'amqp', hostname: 'localhost', port: 5672, username: 'user', password: 'password', vhost: '/' }, function (err, conn) {
  console.log(err, "perro")


  if (err)
    return

  conn.createChannel(function (err, ch) {
    var q = 'hello';

    ch.assertQueue(q, { durable: false });
    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);
    ch.consume(q, function (msg) {
      console.log(" [x] Received %s", msg.content.toString());
    }, { noAck: true });
  });
})


