const DataType = [
  "abstract",
  "string",
  "char",
  "text",
  "number",
  "tinyint",
  "smallint",
  "mediumint",
  "integer",
  "bigint",
  "float",
  "time",
  "date",
  "dateonly",
  "boolean",
  "now",
  "blob",
  "decimal",
  "numeric",
  "uuid",
  "uuidv1",
  "uuidv4",
  "hstore",
  "json",
  "jsonb",
  "virtual",
  "array",
  "none",
  "enum",
  "range",
  "real",
  "double",
  "double precision",
  "geometry",
  "geography",
  "cidr",
  "inet",
  "macaddr"
];


module.exports = {
  helpers: {
    getFields: (field) => {
      const ff = field.split(":")
      return {
        name: ff[0],
        type: ff[1]
      }
    }
  }
}