# Como instalar

```
docker-compose build
docker-compose up
```

una vez que ya se haya terminado habra que dar ctrl+c para cerrar y volver a ejecutar (esto solo deberia ser la primera vez)

```
docker-compose up
```

ahora puedes entrar a localhost y te debera mostrarte la aplicacion

revisar que los puertos de rabbitmq no esten ocupados ya puerto 8080 para entrar a la ui y 5672 donde se conectan los cientes 
para correr rabbitmq ver la documentacion de dockers_configs/rabbitmq/README.md